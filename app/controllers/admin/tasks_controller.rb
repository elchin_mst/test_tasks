class Admin::TasksController < BaseTasksController
  before_action :authenticate_user!
  before_action do redirect_to root_path unless current_user && current_user.admin end

  def index
    @tasks = Task.all
  end

  def edit
    @task = Task.find(params[:id])
  end

  def update
    @task = Task.find(params[:id])
    if @task.update(task_params)
      redirect_to admin_tasks_path
    end
  end

  def destroy
    @task = Task.find(params[:id])
    if @task.destroy
      redirect_to admin_tasks_path
    end
  end

end
