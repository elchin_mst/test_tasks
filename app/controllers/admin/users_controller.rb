class Admin::UsersController < ApplicationController
  before_action :authenticate_user!
  before_action do redirect_to root_path unless current_user && current_user.admin end

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to admin_users_path
    else
      render :new
    end
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      redirect_to admin_users_path
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      redirect_to admin_users_path
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :role, :site, :description, :name, :phone, :company)
  end

end

