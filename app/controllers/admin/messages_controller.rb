class Admin::MessagesController < BaseMessagesController
  before_action :authenticate_user!
  before_action do redirect_to root_path unless current_user && current_user.admin end

  def destroy
    @message = Message.find(params[:id])
    if @message.destroy
      redirect_back(fallback_location: root_path)
    end
  end
end
