class BaseMessagesController < ApplicationController
  def new
    @message = Message.new
  end

  def create
    @task = Task.find(params[:task_id])
    @message = Message.new(message_params.merge(user: current_user, task: @task))
    if @message.save
      redirect_back(fallback_location: root_path)
    end
  end

  private

  def message_params
    params.require(:message).permit(:text_message)
  end
end
