class BaseTasksController < ApplicationController
  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params.merge(user: current_user))
    if @task.save
      redirect_to admin_tasks_path
    else
      render :new
    end
  end

  def show
    @task = Task.find(params[:id])
    @new_message = Message.new
  end

  private
  def task_params
    params.require(:task).permit(:name, :status, :description)
  end
end
