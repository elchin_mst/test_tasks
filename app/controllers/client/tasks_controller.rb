class Client::TasksController < BaseTasksController
  before_action :authenticate_user!

  def index
    @tasks = Task.joins(:user).where('users.company = (?)', current_user.company)
  end

  private
  def task_params
    params.require(:task).permit(:name, :status, :description)
  end
end
