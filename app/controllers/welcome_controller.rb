class WelcomeController < ApplicationController
  layout 'auth_form'
  def index
    if user_signed_in?
      @user = current_user
      if current_user.try(:admin)
        redirect_to admin_tasks_path
      else
        redirect_to client_tasks_path
      end
    end
  end
end
