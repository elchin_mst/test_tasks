class Message < ApplicationRecord
  belongs_to :user
  belongs_to :task
  validates :text_message, length: { maximum: 140 }
end
