class Task < ApplicationRecord
  has_many :messages, dependent: :destroy
  belongs_to :user
  validates :description, :name, :status, presence: true
end
