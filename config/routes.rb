Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index'
  devise_for :users
  namespace :admin do
    resources :users
    resources :tasks
    resources :messages, only: [ :new, :create, :destroy ]
  end
  namespace :client do
    resources :tasks
    resources :messages, only: [ :new, :create ]
  end
end
