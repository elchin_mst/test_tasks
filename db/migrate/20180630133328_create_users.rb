class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :company, null: false
      t.text :description
      t.string :name, null: false
      t.string :phone
      t.string :site
      t.timestamps
    end
  end
end
