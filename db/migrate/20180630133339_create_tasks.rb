class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :name, null: false
      t.string :status, null: false, default: 'new'
      t.text :description, null: false
      t.belongs_to :user, null: false, foreign_key: true
      t.timestamps
    end
  end
end
